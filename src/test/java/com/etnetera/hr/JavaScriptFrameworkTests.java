package com.etnetera.hr;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.etnetera.hr.controller.JavaScriptFrameworkController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests {

	@Autowired
	private MockMvc mockMvc;
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private JavaScriptFrameworkRepository repository;

	@Autowired
	private JavaScriptFrameworkController frameworkController;

	@Before
	public void prepareData() throws Exception {

		JavaScriptFramework react = new JavaScriptFramework("ReactJS", "1.0.0",
				new GregorianCalendar(2015, Calendar.DECEMBER, 12), (byte) 9 );
		JavaScriptFramework vue = new JavaScriptFramework("Vue.js", "1.0.0",
                new GregorianCalendar(2015, Calendar.APRIL, 2), (byte) 7);

		repository.save(react);
		repository.save(vue);

	}
	@After
	public void clearAll(){
		repository.deleteAll();
	}


	@Test
	public void frameworksTest() throws Exception {
		mockMvc.perform(get("/frameworks"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Vue.js")));
	}
	@Test
	public void getByIdTest() throws Exception {
		mockMvc.perform(get("/frameworks/1"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("id", is(1)))
				.andExpect(jsonPath("name", is("ReactJS")));
	}

	@Test
	public void addTest() throws Exception {
		JavaScriptFramework angular = new JavaScriptFramework("Angular", "1.0.0",
				new GregorianCalendar(2016, Calendar.APRIL, 2), (byte) 6);

		mockMvc.perform(post("/frameworks")
				.contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(angular)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("id", is(3)))
				.andExpect(jsonPath("name", is("Angular")));
	}

	@Test
	public void removeTest() throws Exception {
		mockMvc.perform(delete("/frameworks/2")).andExpect(status().isOk());

		mockMvc.perform(get("/frameworks"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")));
	}

	@Test
	public void updateTest() throws Exception {
		JavaScriptFramework brandNew = new JavaScriptFramework("Angular", "1.0.0",
				new GregorianCalendar(2016, Calendar.APRIL, 2), (byte) 6);

		mockMvc.perform(put("/frameworks/3")
				.contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(brandNew)))
				.andExpect(status().isOk());


		JavaScriptFramework hereButModified = new JavaScriptFramework("Vue.js", "1.0.0",
				new GregorianCalendar(2016, Calendar.APRIL, 5), (byte) 6);
		hereButModified.setId(2L);

		mockMvc.perform(put("/frameworks/2")
				.contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(hereButModified)))
				.andExpect(status().isOk());


		hereButModified.setId(4L);
		mockMvc.perform(put("/frameworks/2")
				.contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(hereButModified)))
				.andExpect(status().isOk());

		mockMvc.perform(get("/frameworks"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Vue.js")))
				.andExpect(jsonPath("$[1].hypeLevel", is(6)))
				.andExpect(jsonPath("$[2].id", is(3)))
				.andExpect(jsonPath("$[2].name", is("Angular")));
	}

	@Test
	public void searchByNameTest() throws Exception {
		mockMvc.perform(get("/frameworks/search?name=ReactJS"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")));
	}

	@Test
	public void searchByNameEmptyTest() throws Exception {
		mockMvc.perform(get("/frameworks/search?name=BACKBONE.JS"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}
	@Test
	public void searchByHypeLevelTest() throws Exception {
		mockMvc.perform(get("/frameworks/search?hypeLevel=9"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")));
	}

	@Test
	public void searchByHypeLevelEmptyTest() throws Exception {
		mockMvc.perform(get("/frameworks/search?hypeLevel=0"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void addFrameworkInvalid() throws  Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(status().reason("Something went wrong!"));


		framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
		mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(status().reason("Something went wrong!"));

		framework = new JavaScriptFramework();
		framework.setHypeLevel((byte) 100);
		mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(status().reason("Something went wrong!"));
		
	}
	
}
