package com.etnetera.hr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController {

	private final JavaScriptFrameworkRepository repository;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return repository.findAll();
	}

	private Optional<JavaScriptFramework> find(JavaScriptFramework jsFramework) {
		return StreamSupport.stream(repository.findAll().spliterator(), true)
				.filter(framework -> framework.equals(jsFramework))
				.findFirst();
	}

	@PostMapping("/frameworks")
	public JavaScriptFramework add(@RequestBody JavaScriptFramework jsFramework) {
		try {
			if(find(jsFramework).isEmpty()){
				return repository.save(jsFramework);
			}
			return jsFramework;
		} catch (Exception ex) {
			throw new ResponseStatusException(
					HttpStatus.BAD_REQUEST, "Something went wrong!", ex);
		}

	}

	@DeleteMapping("/frameworks/{id}")
	public void delete(@PathVariable Long id) {
		Optional<JavaScriptFramework> optFramework = repository.findById(id);
		optFramework.ifPresent(repository::delete);
	}

	@PutMapping("/frameworks/{id}")
	public JavaScriptFramework update(@RequestBody JavaScriptFramework newFramework, @PathVariable Long id) {
		Optional<JavaScriptFramework> optFramework = find(newFramework);

		if(optFramework.isPresent() && optFramework.get().getId().equals(id)){
			JavaScriptFramework framework = optFramework.get();
			framework.setDeprecationDate(newFramework.getDeprecationDate());
			framework.setHypeLevel(newFramework.getHypeLevel());
			return repository.save(framework);
		}else{
			newFramework.setId(id);
			return add(newFramework);
		}
	}

	@GetMapping(value = "/frameworks/search", params = {"name"})
	public List<JavaScriptFramework> search(@RequestParam(value = "name") String name) {
		return StreamSupport.stream(repository.findAll().spliterator(), true)
				.filter(framework -> framework.getName().equals(name))
				.collect(Collectors.toList());
	}

	@GetMapping(value = "/frameworks/search", params = {"hypeLevel"})
	public List<JavaScriptFramework> search(@RequestParam(value = "hypeLevel") byte hypeLevel) {
		return StreamSupport.stream(repository.findAll().spliterator(), true)
				.filter(framework -> framework.getHypeLevel() == hypeLevel)
				.collect(Collectors.toList());
	}

	@GetMapping("/frameworks/{id}")
	JavaScriptFramework getById(@PathVariable Long id) {
		return repository.findById(id)
				.orElseThrow(IllegalArgumentException::new);
	}




}
