package com.etnetera.hr.data;



import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Calendar;
import java.util.Objects;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity

public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;


	@Column(name = "name", nullable = false, length = 30)
	private String name;


	@Column(name = "version", nullable = false, length = 30)
	private String version;

	@Column(name = "deprecation_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar deprecationDate;

    @Column(name = "hype_level", nullable = false)
    @Min(0)
    @Max(9)
    private byte hypeLevel;

    public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name, String version, Calendar deprecationDate, byte hypeLevel) {
		this.name = name;
		this.version = version;
		this.deprecationDate = deprecationDate;
		this.hypeLevel = hypeLevel;

	}
	@Override
	public int hashCode() {
		return Objects.hash(id, name, version);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		JavaScriptFramework other = (JavaScriptFramework) o;
		return this.id != null && other.id != null
				&& (this.id.equals(other.id))
				|| ((this.name.equals(other.name))
				&& (this.version.equals(other.version)));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Calendar getDeprecationDate() {
		return deprecationDate;
	}

	public void setDeprecationDate(Calendar deprecationDate) {
		this.deprecationDate = deprecationDate;
	}

	public byte getHypeLevel() {
		return hypeLevel;
	}

	public void setHypeLevel(byte hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + ", name=" + name + "]";
	}

}
